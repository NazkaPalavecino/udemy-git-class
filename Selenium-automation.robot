***Settings**
Library     String
Library     SeleniumLibrary

***Variables***
${Browser}      chrome
${Homepage}     automationpractice.com/index.php
${Scheme}       http
${testUrl}      ${Scheme}://${Homepage}

***Keywords***
Open Homepage
    Open Browser    ${testUrl}      ${Browser}

***Test Cases***
C001 Hacer click en contenedores
    Open Homepage
    Set Global Variable    @{nombresDeContenedores}    //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a    //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    :FOR    ${nombreDeContenedor}   IN      @{nombresDeContenedores}
    \   Run Keyword If      '${nombreDeContenedor}'=='//*[@id="homefeatured"]/li[7]/div/div[2]/h5/a'      Exit For Loop
    \   Click Element       ${nombreDeContenedor}
    \   Wait Until Element Is Visible   xpath=//*[@id="bigpic"]
    \   Click Element       xpath=//*[@id="header_logo"]/a/img
    Close Browser
